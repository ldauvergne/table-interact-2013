﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IvyCom;

namespace SerialPortListenner
{
    public partial class FenetreParametrage : Form
    {
        private ListenerArduino listener;
        private delegate void ConsoleDelegateHandler(String line);
        private ConsoleDelegateHandler ConsoleDelegate;
        private CommunicationIvy ivybus = new CommunicationIvy();

        public FenetreParametrage()
        {
            InitializeComponent();
            ConsoleDelegate = new ConsoleDelegateHandler(ecrireSurConsole);
            this.FormClosing += new FormClosingEventHandler(FenetreParametrage_FormClosing);
            this.Valider.Enabled = true;
            this.stopper.Enabled = false;
        }

        //Fermeture de la fenetre
        void FenetreParametrage_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (listener != null)
            {
                listener.close(); //Fermeture du port
            }
            this.Dispose(); //Fermeture de la fenetre
        }

        //Lance la connexion au port et l'envoi de message sur le bus Ivy
        private void Valider_Click(object sender, EventArgs e)
        {
            //test de connexion a l'arduino
            if (LancerListenerArduino())
            {
                //test de lancement d'ivy
                if (LancerBusIvy())
                {
                    this.Valider.Enabled = false;
                    this.stopper.Enabled = true;
                    listener.ecouter(ivybus); //Lancement de l'ecoute de l'arduino
                }
                else
                {
                    updateConsole("Erreur : Impossible de lancer le bus Ivy");
                }
            }
            else
            {
                updateConsole("Erreur : Impossible de se connecter à l'arduino, generation de données");
                /* On genere des données */
                DataGenerator generateur = new DataGenerator();
                //test de lancement d'ivy
                if (LancerBusIvy())
                {
                    this.Valider.Enabled = false;
                    this.stopper.Enabled = true;
                    generateur.ecouter(ivybus); 
                }
                else
                {
                    updateConsole("Erreur : Impossible de lancer le bus Ivy");
                }
            }
        }

        //Lancement de l'ecoute sur l'arduino
        private Boolean LancerListenerArduino()
        {
            try
            {
                if (listener != null) listener.close();
                String port = this.Port.Text;
                if (port == null || !port.Contains("COM"))
                {
                    updateConsole("Erreur : Le port doit être de la forme : COM<nombre>");
                    return false;
                }
                int baud = Convert.ToInt32(this.Baud.Text);
                listener = new ListenerArduino(port, baud);
                if (listener.open()) updateConsole("Ouverture du port : ok");
                else
                {
                    updateConsole("Erreur : Port déjà utilisé ou impossible à ouvrire");
                    return false;
                }
            }
            catch (Exception)
            {
                updateConsole("Erreur : Baud doit être un chiffre");
                return false;
            }
            return true;
        }

        //Creation du bus Ivy
        private Boolean LancerBusIvy()
        {
            updateConsole("Lancement du bus Ivy");
            if (ivybus.launchIvyBus())
            {
                updateConsole("Bus Ivy Lancé & Abonné au bus Ivy");
                return true;
            }
            else
            {
                updateConsole("La creation du BUS Ivy a échoué");
                return false;
            }
        }

        //Mise a jour de la console
        public void updateConsole(String s)
        {
            try { this.Invoke(this.ConsoleDelegate, new object[] { s }); }
            catch (ObjectDisposedException) { }
        }

        //Routine d'ecriture sur la console
        private void ecrireSurConsole(String s)
        {
            List<string> l = Console.Lines.ToList(); ;
            l.Add(s);
            Console.Lines = l.ToArray();
            Console.SelectionStart = Console.Text.Length;
            Console.SelectionLength = 0;
            Console.ScrollToCaret();
        }

        //Stop la  connexion au port
        private void stopper_Click(object sender, EventArgs e)
        {
            if (listener != null)
            {
                updateConsole("Fermeture du port...");
                listener.close();
                updateConsole("Fermeture du port : ok");
            }
            if (ivybus != null)
            {
                updateConsole("Fermeture du bus de donné ivy...");
                ivybus.closeIvyBus();
                updateConsole("Fermeture du bus ivy : ok");
            }
            updateConsole("Stop ok");
            this.Valider.Enabled = true;
            this.stopper.Enabled = false;
        }
    }
}
