﻿using System;
using System.IO.Ports;
using System.IO;
using System.Threading;
using IvyCom;

namespace SerialPortListenner
{
    class ListenerArduino
    {
        private SerialPort p;
        private Thread ecouteArdui;
        private CommunicationIvy ivybus;

        //Constructeur
        public ListenerArduino(String port, int baud)
        {
            p = new SerialPort(port, baud, Parity.None, 8, StopBits.One);
            p.DtrEnable = true;
        }

        //Ouvre une connexion
        //Retourne : 
        // true : ouverture ok
        // false : ouverture du port impossible
        public Boolean open()
        {
            Boolean erreur_port = false;
            try
            {
                p.Open();
            }
            catch (UnauthorizedAccessException) { erreur_port = true; }
            catch (IOException) { erreur_port = true; }
            catch (ArgumentException) { erreur_port = true; }
            if (erreur_port)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Ferme la connexion
        public void close()
        {
            if (p.IsOpen)
            {
                p.Close();
            }
        }

        //Ecoute de l'arduino
        public void ecouter(CommunicationIvy ivybus)
        {
            this.ivybus = ivybus;
            ecouteArdui = new Thread(new ThreadStart(this.start));
            ecouteArdui.Start();
        }

        //Routine du thread d'ecoute
        private void start()
        {
            //Arret du thread à la fermeture du port
            while (p.IsOpen)
            {
                if (p.BytesToRead > 0)
                {
                    try{
                    String s = p.ReadLine(); //Lecture d'un message
                    
                    String[] tab = s.Split('=');
                    MessageIvy m = new MessageIvy("STArduino", Convert.ToInt32(tab[0]), Convert.ToInt32(tab[1]));
                    if(m.distance!=666) Console.WriteLine(m.ToIvyMessage());
                    ivybus.msgsend(m);
                    }
                    catch (Exception){}
                }
            }
        }
    }
}
