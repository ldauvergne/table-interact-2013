﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IvyCom;
using System.Threading;

namespace SerialPortListenner
{
    class DataGenerator
    {
        private Thread t;
        private CommunicationIvy ivybus;

        //Ecoute de l'arduino
        public void ecouter(CommunicationIvy ivybus)
        {
            this.ivybus = ivybus;
            t = new Thread(new ThreadStart(this.start));
            t.Start();
        }

        private void generate(int[] tab)
        {
            for (int i = 0; i < tab.Length; i++)
            {
                MessageIvy m = new MessageIvy("STArduino", i, tab[i]);
                ivybus.msgsend(m);
            }
        }
        //Routine du thread d'ecoute
        private void start()
        {
            int[] tab = new int[] { 666, 666, 666, 5, 666};
            generate(tab);
            tab = new int[] { 20, 666, 666, 5, 666 };
            generate(tab);
            tab = new int[] { 20, 666, 666, 5, 5 };
            generate(tab);

          /*  int[] tab = new int[] { 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666 };
            //Arret du thread à la fermeture du port
            while (true)
            {
                tab = new int[] { 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666, 666 };

                if (tab[1] == 666)
                {
                    tab[1] = 100;
                }
                while (tab[1]!=5)
                {
                    generate(tab);
                    Thread.Sleep(30);
                    tab[1] --;
                }

                Thread.Sleep(30);
                tab[2] = tab[1];
                generate(tab);

                Thread.Sleep(2000);
                tab[1] = 666;

                generate(tab);

                Thread.Sleep(2000);

                if (tab[5] == 666)
                {
                    tab[5] = 100;
                }
                while (tab[5] != 5)
                {
                    generate(tab);
                    Thread.Sleep(30);
                    tab[5]--;
                }


            }
            */
        }
    }
}

/*


                    for (int j = 0; j < 15; j++)
                    {
                        Thread.Sleep(5);
                        int k;
                        if (i == j)
                        {
                            k = 20;
                        }
                        else
                        {
                            k = 666;
                        }
                        MessageIvy m = new MessageIvy("STArduino", j, k);
                        ivybus.msgsend(m);
                    }
*/