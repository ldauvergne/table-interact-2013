﻿namespace SerialPortListenner
{
    partial class FenetreParametrage
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.stopper = new System.Windows.Forms.Button();
            this.Baud = new System.Windows.Forms.TextBox();
            this.Valider = new System.Windows.Forms.Button();
            this.Port = new System.Windows.Forms.TextBox();
            this.BaudLabel = new System.Windows.Forms.Label();
            this.PortLabel = new System.Windows.Forms.Label();
            this.Console = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.stopper);
            this.splitContainer1.Panel1.Controls.Add(this.Baud);
            this.splitContainer1.Panel1.Controls.Add(this.Valider);
            this.splitContainer1.Panel1.Controls.Add(this.Port);
            this.splitContainer1.Panel1.Controls.Add(this.BaudLabel);
            this.splitContainer1.Panel1.Controls.Add(this.PortLabel);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Console);
            this.splitContainer1.Size = new System.Drawing.Size(284, 261);
            this.splitContainer1.SplitterDistance = 94;
            this.splitContainer1.TabIndex = 0;
            // 
            // stopper
            // 
            this.stopper.Location = new System.Drawing.Point(153, 65);
            this.stopper.Name = "stopper";
            this.stopper.Size = new System.Drawing.Size(75, 23);
            this.stopper.TabIndex = 5;
            this.stopper.Text = "Stopper";
            this.stopper.UseVisualStyleBackColor = true;
            this.stopper.Click += new System.EventHandler(this.stopper_Click);
            // 
            // Baud
            // 
            this.Baud.Location = new System.Drawing.Point(138, 39);
            this.Baud.Name = "Baud";
            this.Baud.Size = new System.Drawing.Size(134, 20);
            this.Baud.TabIndex = 4;
            this.Baud.Text = "115200";
            // 
            // Valider
            // 
            this.Valider.Location = new System.Drawing.Point(52, 65);
            this.Valider.Name = "Valider";
            this.Valider.Size = new System.Drawing.Size(75, 23);
            this.Valider.TabIndex = 3;
            this.Valider.Text = "Lancer";
            this.Valider.UseVisualStyleBackColor = true;
            this.Valider.Click += new System.EventHandler(this.Valider_Click);
            // 
            // Port
            // 
            this.Port.Location = new System.Drawing.Point(138, 12);
            this.Port.Name = "Port";
            this.Port.Size = new System.Drawing.Size(134, 20);
            this.Port.TabIndex = 2;
            this.Port.Text = "COM6";
            // 
            // BaudLabel
            // 
            this.BaudLabel.AutoSize = true;
            this.BaudLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BaudLabel.Location = new System.Drawing.Point(13, 39);
            this.BaudLabel.Name = "BaudLabel";
            this.BaudLabel.Size = new System.Drawing.Size(47, 20);
            this.BaudLabel.TabIndex = 1;
            this.BaudLabel.Text = "Baud";
            // 
            // PortLabel
            // 
            this.PortLabel.AutoSize = true;
            this.PortLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PortLabel.Location = new System.Drawing.Point(13, 13);
            this.PortLabel.Name = "PortLabel";
            this.PortLabel.Size = new System.Drawing.Size(96, 20);
            this.PortLabel.TabIndex = 0;
            this.PortLabel.Text = "Nom du port";
            // 
            // Console
            // 
            this.Console.BackColor = System.Drawing.Color.Black;
            this.Console.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Console.ForeColor = System.Drawing.Color.LimeGreen;
            this.Console.Location = new System.Drawing.Point(4, 4);
            this.Console.Name = "Console";
            this.Console.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.Console.Size = new System.Drawing.Size(277, 156);
            this.Console.TabIndex = 0;
            this.Console.Text = "";
            // 
            // FenetreParametrage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FenetreParametrage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FenetreParametrage";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox Baud;
        private System.Windows.Forms.Button Valider;
        private System.Windows.Forms.TextBox Port;
        private System.Windows.Forms.Label BaudLabel;
        private System.Windows.Forms.Label PortLabel;
        private System.Windows.Forms.RichTextBox Console;
        private System.Windows.Forms.Button stopper;

    }
}