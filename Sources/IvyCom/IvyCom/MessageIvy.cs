﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IvyCom
{
    public class MessageIvy
    {
        public String type;
        public int numeroCapteur;
        public int distance;

        public MessageIvy()
        {
        }

        public MessageIvy(String type, int numeroCapteur, int distance)
        {
            this.type = type;
            this.numeroCapteur = numeroCapteur;
            this.distance = distance;
        }

        //Convertion du message en string de type IvyMessage STArduino
        public String ToIvyMessage()
        {
            return (type + "=" + numeroCapteur + ";" + distance);
        }

      
        //Message acceptes : 
        // - STArduino
        public Boolean RemplireMessage(String message)
        {
            String[] messageDec1 = message.Split('=');
            if (messageDec1.Length == 2)
            {
                String type = messageDec1[0];
                //Message de type STArduino
                if (type.Equals("STArduino"))
                {
                    String[] contenu = (messageDec1[1]).Split(';');
                    if (contenu.Length == 2)
                    {
                        try
                        {
                            this.type = "STArduino";
                            this.numeroCapteur = Convert.ToInt32(contenu[0]);
                            this.distance = Convert.ToInt32(contenu[1]);
                            return true;
                        }
                        catch(Exception) {return false;}
                    }
                }
            }
            return false;
        }


        public override string ToString()
        {
            return "Message [ Capteur : " + this.numeroCapteur + " ; Distance : " + this.distance + "]";
        }
    }
}
