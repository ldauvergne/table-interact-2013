﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IvyBus;

namespace IvyCom
{
    public class CommunicationIvy
    {

        private Ivy bus;
        public delegate void NewMessageEventHandler(object sender, MessageEventArgs e);
        public static event NewMessageEventHandler OnNewMessage;

        //Lancement du bus de donnée Ivy
        public bool launchIvyBus()
        {
            try
            {
                //Dns.GetHostAddresses(Dns.GetHostName()) 
                bus = new Ivy("UnityIvy", "");
                bus.Start("127.255.255.255" + ":" + 9000);
                return true;
                
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Inscription aux messages de type : idchannel
        public bool subcribeIvyMess(String idchannel)
        {
            try
            {
                if (idchannel.Equals("STArduino"))
                {
                    EventHandler<IvyMessageEventArgs> message = new EventHandler<IvyMessageEventArgs>(msgreceiveSTArduino);
                    bus.BindMsg("^" + idchannel + "=(.*)", new EventHandler<IvyMessageEventArgs>(message));
                    return true;
                }
                else if (idchannel.Equals("Presence"))
                {
                    EventHandler<IvyMessageEventArgs> message = new EventHandler<IvyMessageEventArgs>(msgreceivePresence);
                    bus.BindMsg("^" + idchannel + "=(.*)", new EventHandler<IvyMessageEventArgs>(message));
                    return true;
                }
                else if (idchannel.Equals("Position"))
                {
                    EventHandler<IvyMessageEventArgs> message = new EventHandler<IvyMessageEventArgs>(msgreceivePosition);
                    bus.BindMsg("^" + idchannel + "=(.*)", new EventHandler<IvyMessageEventArgs>(message));
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Desinscription aux messages de type : idchannel
        public bool unsubcribeIvyMess(String idchannel)
        {
            try
            {
                bus.UnbindMsg("^" + idchannel + "=(.*)");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Reception d'un message bas niveau
        public void msgreceiveSTArduino(Object o, IvyMessageEventArgs a)
        {
            MessageEventArgs e = new MessageEventArgs("STArduino=" + a.GetArguments()[0]);
            OnNewMessage(this, e);
        }

        //Reception d'un message presence
        public void msgreceivePresence(Object o, IvyMessageEventArgs a)
        {
            MessageEventArgs e = new MessageEventArgs("Presence=" + a.GetArguments()[0]);
            OnNewMessage(this, e);
        }

        //Reception d'un message position
        public void msgreceivePosition(Object o, IvyMessageEventArgs a)
        {
            MessageEventArgs e = new MessageEventArgs("Position=" + a.GetArguments()[0]);
            OnNewMessage(this, e);
        }

        //Envoi d'un message
        public void msgsend(MessageIvy message)
        {
            bus.SendMsg(message.ToIvyMessage());
        }
        //Envoi d'un message Position
        public void msgsend(MessageIvyPosition message)
        {
            bus.SendMsg(message.ToIvyMessage());
        }
        //Envoi d'un message Presence
        public void msgsend(MessageIvyPresence message)
        {
            bus.SendMsg(message.ToIvyMessage());
        }

        //Stop le bus de donnée Ivy
        public void closeIvyBus()
        {
            bus.Stop();
        }
    }
}
