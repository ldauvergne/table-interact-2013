﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IvyCom
{
    public class MessageIvyPresence
    {
        public int id_pers;
        public bool presence;

        public MessageIvyPresence()
        {
        }

        public MessageIvyPresence(int id_pers, bool presence)
        {
            this.id_pers = id_pers;
            this.presence = presence;
        }

        //Convertion du message en string de type IvyMessage STArduino
        public String ToIvyMessage()
        {
            return ("Presence=" + id_pers + ";" + presence);
        }


        //Message acceptes : 
        public Boolean RemplireMessage(String message)
        {
            String[] messageDec1 = message.Split('=');
            if (messageDec1.Length == 2)
            {
                if (messageDec1[0].Equals("Presence"))
                {
                    try
                    {
                        String[] messageDec2 = messageDec1[1].Split(';');
                        this.id_pers = Convert.ToInt32(messageDec2[0]);
                        if (messageDec2[1].Equals("False")) this.presence = false;
                        else this.presence = true;
                        return true;
                    }
                    catch (Exception) { return false; }
                }
            }
            return false;
        }

    }
}
