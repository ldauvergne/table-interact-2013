﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IvyCom
{
    public class MessageIvyPosition
    {
        public int id_pers;
        public int x;
        public int y;
        public int distance;

        public MessageIvyPosition()
        {
        }

        public MessageIvyPosition(int id_pers, int x, int y, int distance)
        {
            this.id_pers = id_pers;
            this.x = x;
            this.y = y;
            this.distance = distance;
        }

        //Convertion du message en string de type IvyMessage STArduino
        public String ToIvyMessage()
        {
            return ("Position=" + id_pers + ";" + x + ";" + y + ";" + distance);
        }


        //Message acceptes : 
        // - STArduino
        public Boolean RemplireMessage(String message)
        {
            String[] messageDec1 = message.Split('=');
            if (messageDec1.Length == 2)
            {
                try
                {
                    String[] contenu = (messageDec1[1]).Split(';');
                    if (contenu.Length == 4)
                    {
                        this.id_pers = Convert.ToInt32(contenu[0]);
                        this.x = Convert.ToInt32(contenu[1]);
                        this.y = Convert.ToInt32(contenu[2]);
                        this.distance = Convert.ToInt32(contenu[3]);
                        return true;
                    }

                }
                catch (Exception) { return false; }

            }
            return false;
        }
    }
}
