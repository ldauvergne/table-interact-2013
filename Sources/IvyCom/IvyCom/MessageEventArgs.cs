﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IvyCom
{
    
        public class MessageEventArgs : EventArgs
	    {
            private String message;

            public MessageEventArgs(String message)
            {
                this.message = message;
            }

            public String Message
            {
                get { return message; }
            }
        }
}
