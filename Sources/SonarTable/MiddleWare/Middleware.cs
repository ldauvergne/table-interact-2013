﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IvyCom;
using System.Collections;
using System.Drawing;


namespace MiddleWare
{
    public class Middleware
    {
        private int nbCapteurLarg;
        private int nbCapteurLong;
        private Point[] positioncapteurs;
        private int tableHeight;
        private int tableWidth;
        public delegate void PersonnePosEventHandler(object sender, PersonnePosEventArgs e);
        public event PersonnePosEventHandler OnPersonnePosChanged;
        public Personnes personnes;
        private int nbAbsenceAcceptee = 5;
        private int distanceDifferenceAcceptee = 10;
        private Buffer buffer;
        CommunicationIvy ivyBus;

        public Middleware(int nbCapteurLarg, int nbCapteurLong)
        {
            ivyBus = new CommunicationIvy();
            ivyBus.launchIvyBus();
            ivyBus.subcribeIvyMess("STArduino");
            CommunicationIvy.OnNewMessage += new CommunicationIvy.NewMessageEventHandler(CommunicationIvy_OnNewMessage);
            this.nbCapteurLarg = nbCapteurLarg;
            this.nbCapteurLong = nbCapteurLong;

            personnes = new Personnes();
            buffer = new Buffer();
            
        }

        private Point getsensorposition(int idsensor)
        {
            idsensor++;

            if ((idsensor - nbCapteurLong) > 0)
            {
                if((idsensor - nbCapteurLong - nbCapteurLarg) > 0)
                {
                    if((idsensor - nbCapteurLong*2 - nbCapteurLarg) > 0)
                    {
                        //On est sur la deuxieme largeur
                        return new Point(0, (int)(tableHeight-(((tableHeight / nbCapteurLarg) * (idsensor - nbCapteurLong * 2 - nbCapteurLarg)) - (tableHeight / nbCapteurLarg) / 2)));
                    }
                    else
                    {
                        //On est sur la deuxième longueur
                        return new Point((int)(tableWidth-(((tableWidth / nbCapteurLong) * (idsensor - nbCapteurLong - nbCapteurLarg)) - (tableWidth / nbCapteurLong) / 2)), tableHeight);
                    }
                }
                else
                {
                    //On est sur la première largeur
                    return new Point(tableWidth, (int)(((tableHeight / nbCapteurLarg) * (idsensor - nbCapteurLong)) - (tableHeight / nbCapteurLarg)/2));
                }
            }
            else
            {
                //On est sur la première longueur
                return new Point((int)(((tableWidth / nbCapteurLong) * idsensor) - (tableWidth / nbCapteurLong)/2), 0);
            }
        }

        public void genererPositionsCapteurs()
        {

            int nbpointstotal=(nbCapteurLarg*2)+(nbCapteurLong*2);
            positioncapteurs = new Point[nbpointstotal];
            
            //Pour chaque point on calcule sa position sur la fenetre et on stocke le résultat
            for (int i=0;i<nbpointstotal;i++)
            {
                positioncapteurs[i]=getsensorposition(i);
                //Console.WriteLine(this.getsensorposition(i).X + " " + this.getsensorposition(i).Y);
            }
        }

        //Génére la position de l'utilisateur selon sa liste de capteurs
        private void genererPositionPersonne(Personne pers)
        {
            //On parcours la hastable en récupérant les clés qu'on stocke dans un tableau

            List<int> idcapteurs = new List<int>(); 

            foreach (DictionaryEntry entry in pers.capteurs)
            {
                if (((int)entry.Key) < positioncapteurs.Length)
                {
                    idcapteurs.Add((int)entry.Key);
                }
            }

            //On trie le tableau
            idcapteurs.Sort();

            //On selectionne de premier et le dernier capteur et on fait la moyenne des deux
            pers.x = (int)((positioncapteurs[idcapteurs[0]].X + positioncapteurs[idcapteurs[idcapteurs.Count - 1]].X) / 2);
            pers.y = (int)((positioncapteurs[idcapteurs[0]].Y + positioncapteurs[idcapteurs[idcapteurs.Count - 1]].Y) / 2);
        }

        private void CommunicationIvy_OnNewMessage(object sender, MessageEventArgs e)
        {
            String bufferType = "STArduino";
            MessageIvy m = new MessageIvy();
            m.RemplireMessage(e.Message);

            Console.WriteLine();
            //Le capteur ne detecte personne
            if (m.distance == 666)
            {
                Console.WriteLine("Le capteur ne detecte personne");
                Personne p = personnes.getPersonneByCapteur(m.numeroCapteur);
                //Ce capteur appartient à une personne
                if (p != null)
                {
                    Console.WriteLine("Ce capteur appartient à la personne : "+p.identifiant);
                    //Ajout au buffer la valeur du capteur
                    buffer.addsensorvalue(bufferType, m.numeroCapteur, m.distance);
                    //Le buffer contient trop de message d'abscence 
                    if (buffer.nbSensorValue(bufferType, m.numeroCapteur) > this.nbAbsenceAcceptee)
                    {
                        buffer.clearSensorValue(bufferType, m.numeroCapteur);
                        Console.WriteLine("Suppression du capteur "+m.numeroCapteur+ "de" + p.identifiant);
                        //Suppression du capteur seulement si il se trouve à l'extremité de la liste de l'utilisateur
                        if(p.capteurAuExtremite(m.numeroCapteur))
                        {
                            p.supprimerCapteur(m.numeroCapteur);
                            //Suppression de la personne si elle n'a plus de capteur dans sa liste
                            if (!p.hasCateurs())
                            {
                                Console.WriteLine("Suppression de la personne "+p.identifiant);
                                PersonnePosEventArgs evenement = new PersonnePosEventArgs(p.identifiant, true);
                                OnPersonnePosChanged(this, evenement);
                                personnes.supprimerPersonne(p);
                            }
                            else
                            {
                                genererPositionPersonne(p);
                                PersonnePosEventArgs evenement = new PersonnePosEventArgs(p.identifiant, false);
                                OnPersonnePosChanged(this, evenement);
                            }
                        }

                    }
                }
                
            }
            //Le capteur detecte quelqu'un
            else
            {
                Console.WriteLine("Le capteur detecte quelqu'un");
                //Suppression des messages d'abscence du buffer
                buffer.clearSensorValue(bufferType, m.numeroCapteur);
                //Aucune personne ne possède ce capteur
                Personne p = personnes.getPersonneByCapteur(m.numeroCapteur);
                if (p == null)
                {
                    Console.WriteLine("Personne ne possède ce capteur");
                    Personne p_proche = personnes.getPersonneByCapteurProche(m.numeroCapteur);
                    //Aucune personne proche du capteur
                    if (p_proche == null)
                    {
                        Console.WriteLine("Aucune personne proche du capteur");
                        Console.WriteLine("Creation d'une nouvelle personne");
                        Personne new_p = new Personne();
                        new_p.ajouterCapteur(m.numeroCapteur,m.distance);
                        personnes.addPersonne(new_p);
                        genererPositionPersonne(new_p);
                        //Envoi de l'evenement
                        PersonnePosEventArgs evenement = new PersonnePosEventArgs(new_p.identifiant,false);
                        OnPersonnePosChanged(this, evenement);
                    }
                    //Une personne est proche du capteur
                    else
                    {
                        Console.WriteLine("Une personne est proche du capteur : "+p_proche.identifiant);
                        //La personne est à une distance proche de celle signalée par le capteur
                        if (Math.Abs(m.distance - p_proche.get_distance()) < distanceDifferenceAcceptee)
                        {
                            Console.WriteLine("La personne est à une distance proche de celle signalée par le capteur");
                            p_proche.ajouterCapteur(m.numeroCapteur, m.distance);
                            genererPositionPersonne(p_proche);
                            //Envoi de l'evenement
                            PersonnePosEventArgs evenement = new PersonnePosEventArgs(p_proche.identifiant,false);
                            OnPersonnePosChanged(this, evenement);
                        }
                        //La personne est eloignée de la distance proposé
                        else
                        {
                            Console.WriteLine("La personne est eloignée de la distance proposé");
                            Console.WriteLine("Creation d'une nouvelle personne");
                            Personne new_p = new Personne();
                            new_p.ajouterCapteur(m.numeroCapteur, m.distance);
                            personnes.addPersonne(new_p);
                            genererPositionPersonne(new_p);
                            //Envoi de l'evenement
                            PersonnePosEventArgs evenement = new PersonnePosEventArgs(new_p.identifiant,false);
                            OnPersonnePosChanged(this, evenement);
                        }
                    }
                }
                //Une personne possède ce capteur dans sa liste
                else
                {
                    Console.WriteLine("Une personne possède ce capteur dans sa liste : "+p.identifiant);
                    //La personne est à une distance proche de celle signalée par le capteur
                    if (Math.Abs(m.distance - p.get_distance()) < distanceDifferenceAcceptee)
                    {
                        Console.WriteLine("La personne est à une distance proche de celle signalée par le capteur");
                        p.updateCapteur(m.numeroCapteur, m.distance);
                        //Envoi de l'evenement
                        PersonnePosEventArgs evenement = new PersonnePosEventArgs(p.identifiant,false);
                        OnPersonnePosChanged(this, evenement);
                    }
                    //La personne est eloignée de la distance proposé
                    else
                    {
                        Console.WriteLine("La personne est eloignée de la distance proposé, supp du capteur");
                        p.supprimerCapteur(m.numeroCapteur);
                        //Envoi de l'evenement
                        if (!p.hasCateurs())
                        {
                            Console.WriteLine("Suppression de la personne " + p.identifiant);
                            PersonnePosEventArgs evenement = new PersonnePosEventArgs(p.identifiant, true);
                            OnPersonnePosChanged(this, evenement);
                            personnes.supprimerPersonne(p);
                        }
                        else
                        {
                            Console.WriteLine("Update de la personne " + p.identifiant);
                            genererPositionPersonne(p);
                            PersonnePosEventArgs evenement = new PersonnePosEventArgs(p.identifiant, false);
                            OnPersonnePosChanged(this, evenement);
                        }
                        
                        Personne new_p = new Personne();
                        Console.WriteLine("Ajout d'une personne " + new_p.identifiant);
                        new_p.ajouterCapteur(m.numeroCapteur, m.distance);
                        personnes.addPersonne(new_p);
                        genererPositionPersonne(new_p);
                        //Envoi de l'evenement
                        PersonnePosEventArgs evenement2 = new PersonnePosEventArgs(new_p.identifiant,false);
                        OnPersonnePosChanged(this, evenement2);
                    }
                }
            }
        }

        public void changesize(double tableHeight, double tableWidth)
        {
            this.tableWidth = (int)tableWidth;
            this.tableHeight = (int)tableHeight;

            genererPositionsCapteurs();

            Console.WriteLine(this.tableWidth + " " + this.tableHeight);

            Console.WriteLine(this.getsensorposition(3).X + " " + this.getsensorposition(3).Y);

            //REGENERER LES POSITIONS DES PERSONNES - MARCHE PAS
            foreach (DictionaryEntry entry in personnes.liste_personnes)
            {
                Console.WriteLine(((Personne)entry.Value).x);
                genererPositionPersonne((Personne)entry.Value);
                Console.WriteLine(((Personne)entry.Value).x);
            }
            /*
             * Console.WriteLine(this.tableWidth + " " + this.tableHeight);

            Console.WriteLine(this.getsensorposition(3).X + " " + this.getsensorposition(3).Y);
            Console.WriteLine(this.getsensorposition(13).X + " " + this.getsensorposition(13).Y);
            Console.WriteLine(this.getsensorposition(18).X + " " + this.getsensorposition(18).Y);
            Console.WriteLine(this.getsensorposition(28).X + " " + this.getsensorposition(28).Y);
            */
        }
    }
}
