﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiddleWare
{
    public class PersonnePosEventArgs : EventArgs
    {
        private int identifiant_personne;
        private Boolean needToBeDelete;

        public PersonnePosEventArgs(int identifiant_personne, Boolean needToBeDelete)
		{
            this.identifiant_personne = identifiant_personne; 
            this.needToBeDelete = needToBeDelete;
		}

        public int Identifiant_personne
		{
            get { return this.identifiant_personne; }
		}

        public Boolean NeedToBeDelete
        {
            get { return needToBeDelete; }
        }
    }
}
