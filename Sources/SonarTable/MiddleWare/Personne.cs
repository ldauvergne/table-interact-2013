﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace MiddleWare
{
    public class Personne
    {
        public int identifiant;
        private int distance;
        public int x=0;
        public int y=0;
        public Hashtable capteurs;

        public Personne()
        {
            this.distance = -1;
            capteurs = new Hashtable();
        }

        //Genere la distance de la personne par rapport à la table en fesant la moyenne des valeurs des capteurs
        private void genererDistance()
        {
            if (capteurs.Count > 0)
            {
                int distance_cumulee = 0;
                foreach (DictionaryEntry entry in capteurs)
                {
                    distance_cumulee = distance_cumulee + (int)entry.Value;
                }
                distance = distance_cumulee / capteurs.Count;
            }
            else
            {
                distance = -1;
            }
        }

        //Retourne :
        //vrai si la personne possède le capteur numero_capteur
        //faux sinon
        public Boolean hasCapteur(int numero_capteur)
        {
            return (capteurs.Contains(numero_capteur));
        }

        //Retourne : 
        //Vrai si la personne possède des capteurs
        //Faux sinon
        public Boolean hasCateurs()
        {
            return (capteurs.Count > 0);
        }

        //Supprimer un capteur de la liste de capteurs de la personne
        public void supprimerCapteur(int numero_capteur)
        {
            if (capteurs.Contains(numero_capteur))
            {
                capteurs.Remove(numero_capteur);
                this.genererDistance();
            }
        }

        //Ajoute un capteur à la liste de capteurs de la personne 
        public void ajouterCapteur(int numero_capteur, int distance_capteur)
        {
            capteurs.Add(numero_capteur,distance_capteur);
            this.genererDistance();
        }

        //Retourne : 
        //Vrai si le capteur numero_capteur est à l'extrémité des capteurs de la personne
        //Faux sinon
        public Boolean possedeCapteurProche(int numero_capteur)
        {
            if (capteurs.Count > 0)
            {
                int max = -1;
                int min = -1;
                foreach (DictionaryEntry entry in capteurs)
	            {
                    int i = (int)entry.Key;
                    if (max == -1 && min == -1)
                    {
                        max = i;
                        min = i;
                    }
                    else if (i < min) min = i;
                    else if (i > max) max = i;
                }
                if (max + 1 == numero_capteur) return true;
                else if (min - 1 == numero_capteur) return true;
                else return false;
            }
            else
            {
                return false;
            }
        }

        //Retourne la distance par rapport à la table de la personne
        public int get_distance()
        {
            return this.distance;
        }

        //Mise à jour du capteur numero_capteur
        public void updateCapteur(int numero_capteur, int distance)
        {
            if (capteurs.ContainsKey(numero_capteur))
            {
                capteurs[numero_capteur] = distance;
                this.genererDistance();
            }
        }

        //Retourne un string d'affichage pour les capteurs
        private string afficherCapteurs()
        {
            String s = "(";
            int i = 0;
            foreach (DictionaryEntry entry in capteurs)
            {
                s = s + entry.Key;
                if (i < capteurs.Count-1) s = s + ",";
                i++;
            }
            
            s = s + ")";
            return s;
        }

        //ToString
        public override string ToString()
        {
            return "Personne " + identifiant + " : " + afficherCapteurs();
        }

        //Retourne : 
        //Vrai si le capteur numero_capteur ce trouve au extremité de la liste de capteurs de la personne
        //Faux sinon
        public Boolean capteurAuExtremite(int numero_capteur)
        {
            if (capteurs.Count > 0)
            {
                int max = -1;
                int min = -1;
                foreach (DictionaryEntry entry in capteurs)
                {
                    int i = (int)entry.Key;
                    if (max == -1 && min == -1)
                    {
                        max = i;
                        min = i;
                    }
                    else if (i < min) min = i;
                    else if (i > max) max = i;
                }
                if (max == numero_capteur) return true;
                else if (min == numero_capteur) return true;
                else return false;
            }
            else
            {
                return false;
            }
        }
    }
}
