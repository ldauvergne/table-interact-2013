using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Surface;
using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;
using System.IO.Ports;
using System.IO;
using System.Collections;
using IvyCom;

namespace SonarTable
{

    public partial class MainWindows : Window
    {
        private Hashtable liste_fenetres;
        CommunicationIvy ivybus;

        
        public MainWindows()
        {
            InitializeComponent();
            AddWindowAvailabilityHandlers();
            liste_fenetres = new Hashtable();
            ivybus = new CommunicationIvy();
            ivybus.launchIvyBus();
            ivybus.subcribeIvyMess("Presence");
            ivybus.subcribeIvyMess("Position");
            CommunicationIvy.OnNewMessage += new CommunicationIvy.NewMessageEventHandler(CommunicationIvy_OnNewMessage);
        }

        //A l'arriver d'un message sur le bus Ivy
        private void CommunicationIvy_OnNewMessage(object sender, MessageEventArgs e)
        {
            String[] message = e.Message.Split('=');
            if (message.Length > 0)
            {
                //Message de type Presence
                if (message[0].Equals("Presence"))
                {
                    //Creation d'un message de type presence
                    MessageIvyPresence presence_m = new MessageIvyPresence();
                    presence_m.RemplireMessage(e.Message);
                    //Suppresion de la fenetre si elle existe
                    if (!presence_m.presence && liste_fenetres.ContainsKey(presence_m.id_pers))
                        suppresionFenetre(presence_m);
                }
                //Message de type Position
                else if (message[0].Equals("Position"))
                {
                    //Creation d'un message de type position
                    MessageIvyPosition position_m = new MessageIvyPosition();
                    position_m.RemplireMessage(e.Message);
                    //Ajout de la fenetre si elle n'existe pas
                    if (!liste_fenetres.ContainsKey(position_m.id_pers))
                        ajouterFenetre(position_m);
                    //Mise � jour de la fenetre
                    updateFenetre(position_m);
                }
            }
        }

        //Supprime une fenetre du bureau
        private void suppresionFenetre(MessageIvyPresence position_m)
        {
            Fenetre f = (Fenetre)liste_fenetres[position_m.id_pers];
            this.Bureau.Items.Remove(f);
            liste_fenetres.Remove(position_m.id_pers);
        }

        //Ajoute une fenetre au bureau
        private void ajouterFenetre(MessageIvyPosition position_m)
        {
            Fenetre f_new = new Fenetre();
            f_new.Center = new Point(position_m.x, position_m.y);
            liste_fenetres.Add(position_m.id_pers, f_new);
            this.Bureau.Items.Add(f_new);
        }

        //Met � jour une fenetre donn�e sur le bureau
        private void updateFenetre(MessageIvyPosition position_m)
        {
            Fenetre f = (Fenetre)liste_fenetres[position_m.id_pers];
            f.UpdateLabel("Personne id = " + position_m.id_pers, "Position = " + position_m.x + "/" + position_m.y, "Distance = " + position_m.distance);
            genererOrientation(f, position_m.x, position_m.y);
            genererPosition(f, position_m.x, position_m.y);
        }

        //Generre l'orientation de la fenetre celon la valeurs re�u par les capteurs
        private void genererPosition(Fenetre f, int x, int y)
        {
            if (f.Orientation == 0)
                f.Center = new Point(x, y - (f.ActualWidth / 2));
            else if (f.Orientation == 90)
                f.Center = new Point(x + (f.ActualWidth / 2), y);
            else if(f.Orientation == 180)
                f.Center = new Point(x, y + (f.ActualHeight / 2));
            else if (f.Orientation == 270)
                f.Center = new Point(x - (f.ActualHeight / 2),y);
        }

        //Generre l'orientation de la fenetre celon la valeurs re�u par les capteurs
        private void genererOrientation(Fenetre f, int x, int y)
        {
            //Orientation simple
            if (x == 0) f.Orientation = 90;
            else if (y == 0) f.Orientation = 180;
            else if (x == this.Width) f.Orientation = 270;
            else if (y == this.Height) f.Orientation = 0;
            //Orientation dans les coins
            else
            {
                if (x < this.Width / 2)
                {
                    if (y < this.Height / 2)
                        f.Orientation = 135;
                    else
                        f.Orientation = 45;
                }
                else
                {
                    if (y < this.Height / 2)
                        f.Orientation = 225;
                    else
                        f.Orientation = 315;
                }
            }
        }

        /// <summary>
        /// Occurs when the window is about to close. 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            ivybus.closeIvyBus();
            // Remove handlers for window availability events
            RemoveWindowAvailabilityHandlers();
        }

        /// <summary>
        /// Adds handlers for window availability events.
        /// </summary>
        private void AddWindowAvailabilityHandlers()
        {
            // Subscribe to surface window availability events
            ApplicationServices.WindowInteractive += OnWindowInteractive;
            ApplicationServices.WindowNoninteractive += OnWindowNoninteractive;
            ApplicationServices.WindowUnavailable += OnWindowUnavailable;
        }

        /// <summary>
        /// Removes handlers for window availability events.
        /// </summary>
        private void RemoveWindowAvailabilityHandlers()
        {
            // Unsubscribe from surface window availability events
            ApplicationServices.WindowInteractive -= OnWindowInteractive;
            ApplicationServices.WindowNoninteractive -= OnWindowNoninteractive;
            ApplicationServices.WindowUnavailable -= OnWindowUnavailable;
        }

        /// <summary>
        /// This is called when the user can interact with the application's window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowInteractive(object sender, EventArgs e)
        {
            //TODO: enable audio, animations here
        }

        /// <summary>
        /// This is called when the user can see but not interact with the application's window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowNoninteractive(object sender, EventArgs e)
        {
            //TODO: Disable audio here if it is enabled

            //TODO: optionally enable animations here
        }

        /// <summary>
        /// This is called when the application's window is not visible or interactive.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowUnavailable(object sender, EventArgs e)
        {
            //TODO: disable audio, animations here
        }
    }
}