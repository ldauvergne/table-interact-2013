﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace BundlePP
{
    public class Personnes
    {
        public Hashtable liste_personnes;
        private int nbPersonnes;

        public Personnes()
        {
            liste_personnes = new Hashtable();
            nbPersonnes = 0;
        }

        
        //Retourne la personne d'identifiant identifiant_personne
        public Personne getPersonneById(int identifiant_personne)
        {
            if (liste_personnes.ContainsKey(identifiant_personne))
            {
                return (Personne)this.liste_personnes[identifiant_personne];
            }
            else
            {
                return null;
            }
        }

        //Retourne la première personne possèdant le capteur numeroCapteur
        public Personne getPersonneByCapteur(int numeroCapteur)
        {
            if (nbPersonnes > 0)
            {
                for (int i = 1; i <= nbPersonnes; i++)
                {
                    if (liste_personnes.ContainsKey(i))
                    {
                        if (((Personne)liste_personnes[i]).hasCapteur(numeroCapteur))
                        {
                            return (Personne)liste_personnes[i];
                        }
                    }
                }
            }
            return null;
        }

        //Retourne la première personne possèdant le capteur numeroCapteur à ces extrémité. (gauche ou droite)
        public Personne getPersonneByCapteurProche(int numeroCapteur, int reglageInterval)
        {
            for (int i = 1; i <= nbPersonnes; i++)
            {
                if (liste_personnes.ContainsKey(i))
                {
                    if (((Personne)liste_personnes[i]).possedeCapteurProche(numeroCapteur, reglageInterval))
                    {
                        return (Personne)liste_personnes[i];
                    }
                }
            }
            return null;
        }

        //Ajoute la personne p à la liste de personne
        public void addPersonne(Personne p)
        {
            nbPersonnes++;
            p.identifiant = nbPersonnes;
            liste_personnes.Add(nbPersonnes, p);
        }

        //Supprimer la personne p de la liste de personne
        public void supprimerPersonne(Personne p)
        {
            if (liste_personnes.ContainsKey(p.identifiant))
            {
                liste_personnes.Remove(p.identifiant);
            }
        }
    }
}
