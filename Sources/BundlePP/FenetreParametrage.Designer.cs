﻿namespace BundlePP
{
    partial class FenetreParametrage
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.stopper = new System.Windows.Forms.Button();
            this.Valider = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textDimLargeur = new System.Windows.Forms.TextBox();
            this.labelLargeur = new System.Windows.Forms.Label();
            this.textDimLongueur = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.GroupeNbCapteur = new System.Windows.Forms.GroupBox();
            this.textNbLargeur = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textNbLongueur = new System.Windows.Forms.TextBox();
            this.TextConsole = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.GroupeNbCapteur.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.stopper);
            this.splitContainer1.Panel1.Controls.Add(this.Valider);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.GroupeNbCapteur);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.TextConsole);
            this.splitContainer1.Size = new System.Drawing.Size(284, 285);
            this.splitContainer1.SplitterDistance = 208;
            this.splitContainer1.TabIndex = 0;
            // 
            // stopper
            // 
            this.stopper.Location = new System.Drawing.Point(155, 176);
            this.stopper.Name = "stopper";
            this.stopper.Size = new System.Drawing.Size(75, 23);
            this.stopper.TabIndex = 7;
            this.stopper.Text = "Stopper";
            this.stopper.UseVisualStyleBackColor = true;
            this.stopper.Click += new System.EventHandler(this.stopper_Click);
            // 
            // Valider
            // 
            this.Valider.Location = new System.Drawing.Point(54, 176);
            this.Valider.Name = "Valider";
            this.Valider.Size = new System.Drawing.Size(75, 23);
            this.Valider.TabIndex = 6;
            this.Valider.Text = "Lancer";
            this.Valider.UseVisualStyleBackColor = true;
            this.Valider.Click += new System.EventHandler(this.Valider_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textDimLargeur);
            this.groupBox2.Controls.Add(this.labelLargeur);
            this.groupBox2.Controls.Add(this.textDimLongueur);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(13, 76);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(259, 94);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dimension écran";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(223, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 16);
            this.label6.TabIndex = 13;
            this.label6.Text = "px";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(223, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "px";
            // 
            // textDimLargeur
            // 
            this.textDimLargeur.Location = new System.Drawing.Point(101, 53);
            this.textDimLargeur.Name = "textDimLargeur";
            this.textDimLargeur.Size = new System.Drawing.Size(116, 20);
            this.textDimLargeur.TabIndex = 11;
            this.textDimLargeur.Text = "500";
            // 
            // labelLargeur
            // 
            this.labelLargeur.AutoSize = true;
            this.labelLargeur.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLargeur.Location = new System.Drawing.Point(6, 54);
            this.labelLargeur.Name = "labelLargeur";
            this.labelLargeur.Size = new System.Drawing.Size(54, 16);
            this.labelLargeur.TabIndex = 10;
            this.labelLargeur.Text = "Largeur";
            // 
            // textDimLongueur
            // 
            this.textDimLongueur.Location = new System.Drawing.Point(101, 27);
            this.textDimLongueur.Name = "textDimLongueur";
            this.textDimLongueur.Size = new System.Drawing.Size(116, 20);
            this.textDimLongueur.TabIndex = 9;
            this.textDimLongueur.Text = "900";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Longueur";
            // 
            // GroupeNbCapteur
            // 
            this.GroupeNbCapteur.Controls.Add(this.textNbLargeur);
            this.GroupeNbCapteur.Controls.Add(this.label2);
            this.GroupeNbCapteur.Controls.Add(this.label1);
            this.GroupeNbCapteur.Controls.Add(this.textNbLongueur);
            this.GroupeNbCapteur.Location = new System.Drawing.Point(13, 12);
            this.GroupeNbCapteur.Name = "GroupeNbCapteur";
            this.GroupeNbCapteur.Size = new System.Drawing.Size(259, 58);
            this.GroupeNbCapteur.TabIndex = 4;
            this.GroupeNbCapteur.TabStop = false;
            this.GroupeNbCapteur.Text = "Nombres de capteurs";
            // 
            // textNbLargeur
            // 
            this.textNbLargeur.Location = new System.Drawing.Point(210, 24);
            this.textNbLargeur.Name = "textNbLargeur";
            this.textNbLargeur.Size = new System.Drawing.Size(39, 20);
            this.textNbLargeur.TabIndex = 8;
            this.textNbLargeur.Text = "10";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(150, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Largeur";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Longueur";
            // 
            // textNbLongueur
            // 
            this.textNbLongueur.Location = new System.Drawing.Point(76, 23);
            this.textNbLongueur.Name = "textNbLongueur";
            this.textNbLongueur.Size = new System.Drawing.Size(39, 20);
            this.textNbLongueur.TabIndex = 1;
            this.textNbLongueur.Text = "20";
            // 
            // TextConsole
            // 
            this.TextConsole.BackColor = System.Drawing.SystemColors.WindowText;
            this.TextConsole.ForeColor = System.Drawing.Color.Chartreuse;
            this.TextConsole.Location = new System.Drawing.Point(4, 3);
            this.TextConsole.Multiline = true;
            this.TextConsole.Name = "TextConsole";
            this.TextConsole.Size = new System.Drawing.Size(277, 67);
            this.TextConsole.TabIndex = 0;
            // 
            // FenetreParametrage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 285);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FenetreParametrage";
            this.Text = "BundleBB";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.GroupeNbCapteur.ResumeLayout(false);
            this.GroupeNbCapteur.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textDimLargeur;
        private System.Windows.Forms.Label labelLargeur;
        private System.Windows.Forms.TextBox textDimLongueur;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox GroupeNbCapteur;
        private System.Windows.Forms.TextBox textNbLargeur;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textNbLongueur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button stopper;
        private System.Windows.Forms.Button Valider;
        private System.Windows.Forms.TextBox TextConsole;

    }
}