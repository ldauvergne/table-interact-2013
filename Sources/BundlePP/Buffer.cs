﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IvyCom;
using System.Collections;

namespace BundlePP
{
    public class Buffer
    {
        private Hashtable table;

        //Constructeur
        public Buffer()
        {
            table = new Hashtable();
        }

        //Ajoute un type a la table
        private bool addtype(string type)
        {
            if (table.ContainsKey(type))
            {
                return true;
            }
            else
            {
                try
                {
                    table.Add(type, new Hashtable());
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        //Ajoute un capteur à un type de la table
        private bool addsensor(string typesensor, int sensor)
        {
            //On essaie d'ajouter le type de capteur
            if (this.addtype(typesensor))
            {
                try
                {
                    //On check si le numéro de capteur n'existe pas déjà
                    if (!((Hashtable)table[typesensor]).ContainsKey(sensor))
                    {
                        ((Hashtable)table[typesensor]).Add(sensor, new LinkedList<int>());
                    }
                    //On a bien tout ajouté (ou ca existait déja, on retourne true)
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        //Ajoute une valeur à un capteur
        public bool addsensorvalue(string typesensor, int sensor, int value)
        {
            if (!addsensor(typesensor, sensor))
            {
                return false;
            }
            try
            {
                LinkedList<int> liste = ((LinkedList<int>)((Hashtable)((Hashtable)table[typesensor]))[sensor]);
                liste.AddLast(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Suppprime les valeurs d'un capteurs
        public void clearSensorValue(string typesensor, int sensor)
        {
            if (table.ContainsKey(typesensor))
            {
                if (((Hashtable)table[typesensor]).ContainsKey(sensor))
                {
                    ((LinkedList<int>)((Hashtable)((Hashtable)table[typesensor]))[sensor]).Clear();
                }
            }
            
        }

        //Nombre de valeurs d'un capteurs stocké
        public int nbSensorValue(string typesensor, int sensor)
        {
            try
            {
                return ((LinkedList<int>)((Hashtable)((Hashtable)table[typesensor]))[sensor]).Count();
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
