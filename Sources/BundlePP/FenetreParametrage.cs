﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IvyCom;
using System.Collections;

namespace BundlePP
{
    public partial class FenetreParametrage : Form
    {
        private delegate void ConsoleDelegateHandler(String line);
        private ConsoleDelegateHandler ConsoleDelegate;
        private CommunicationIvy ivybus = new CommunicationIvy();
        public Personnes personnes;
        private int nbAbsenceAcceptee = 8;
        private int distanceDifferenceAcceptee = 100;
        private int reglageInterval = 1;
        private Buffer buffer;

        private int nbCapteurLarg;
        private int nbCapteurLong;
        private Point[] positioncapteurs;
        private int tableHeight;
        private int tableWidth;

        public FenetreParametrage()
        {
            InitializeComponent();
            ConsoleDelegate = new ConsoleDelegateHandler(ecrireSurConsole);
            this.FormClosing += new FormClosingEventHandler(FenetreParametrage_FormClosing);
            this.Valider.Enabled = true;
            this.stopper.Enabled = false;
            personnes = new Personnes();
            buffer = new Buffer();
        }

        //Fermeture de la fenetre
        void FenetreParametrage_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Dispose(); //Fermeture de la fenetre
        }

        //Lance la connexion au port et l'envoi de message sur le bus Ivy
        private void Valider_Click(object sender, EventArgs e)
        {
            //test de lancement d'ivy
            if (LancerBusIvy())
            {
                nbCapteurLarg = Convert.ToInt32(this.textNbLargeur.Text);
                nbCapteurLong = Convert.ToInt32(this.textNbLongueur.Text);
                tableHeight = Convert.ToInt32(this.textDimLargeur.Text);
                tableWidth = Convert.ToInt32(this.textDimLongueur.Text);
                genererPositionsCapteurs();
                this.Valider.Enabled = false;
                this.stopper.Enabled = true;
            }
            else
            {
                updateConsole("Erreur : Impossible de lancer le bus Ivy");
            }
        }

        private Point getsensorposition(int idsensor)
        {
            idsensor++;

            if ((idsensor - nbCapteurLong) > 0)
            {
                if ((idsensor - nbCapteurLong - nbCapteurLarg) > 0)
                {
                    if ((idsensor - nbCapteurLong * 2 - nbCapteurLarg) > 0)
                    {
                        //On est sur la deuxieme largeur
                        return new Point(0, (int)(tableHeight - (((tableHeight / nbCapteurLarg) * (idsensor - nbCapteurLong * 2 - nbCapteurLarg)) - (tableHeight / nbCapteurLarg) / 2)));
                    }
                    else
                    {
                        //On est sur la deuxième longueur
                        return new Point((int)(tableWidth - (((tableWidth / nbCapteurLong) * (idsensor - nbCapteurLong - nbCapteurLarg)) - (tableWidth / nbCapteurLong) / 2)), tableHeight);
                    }
                }
                else
                {
                    //On est sur la première largeur
                    return new Point(tableWidth, (int)(((tableHeight / nbCapteurLarg) * (idsensor - nbCapteurLong)) - (tableHeight / nbCapteurLarg) / 2));
                }
            }
            else
            {
                //On est sur la première longueur
                return new Point((int)(((tableWidth / nbCapteurLong) * idsensor) - (tableWidth / nbCapteurLong) / 2), 0);
            }
        }

        public void genererPositionsCapteurs()
        {

            int nbpointstotal = (nbCapteurLarg * 2) + (nbCapteurLong * 2);
            positioncapteurs = new Point[nbpointstotal];

            //Pour chaque point on calcule sa position sur la fenetre et on stocke le résultat
            for (int i = 0; i < nbpointstotal; i++)
            {
                positioncapteurs[i] = getsensorposition(i);
                //Console.WriteLine(this.getsensorposition(i).X + " " + this.getsensorposition(i).Y);
            }
        }

        //Génére la position de l'utilisateur selon sa liste de capteurs
        private void genererPositionPersonne(Personne pers)
        {
            //On parcours la hastable en récupérant les clés qu'on stocke dans un tableau

            List<int> idcapteurs = new List<int>();

            foreach (DictionaryEntry entry in pers.capteurs)
            {
                if (((int)entry.Key) < positioncapteurs.Length)
                {
                    idcapteurs.Add((int)entry.Key);
                }
            }

            //On trie le tableau
            idcapteurs.Sort();

            //On selectionne de premier et le dernier capteur et on fait la moyenne des deux
            pers.x = (int)((positioncapteurs[idcapteurs[0]].X + positioncapteurs[idcapteurs[idcapteurs.Count - 1]].X) / 2);
            pers.y = (int)((positioncapteurs[idcapteurs[0]].Y + positioncapteurs[idcapteurs[idcapteurs.Count - 1]].Y) / 2);
        }

        //Creation du bus Ivy
        private Boolean LancerBusIvy()
        {
            updateConsole("Lancement du bus Ivy");
            if (ivybus.launchIvyBus())
            {
                updateConsole("Bus Ivy Lancé & Abonné au bus Ivy");
                ivybus.subcribeIvyMess("STArduino");
                CommunicationIvy.OnNewMessage += new CommunicationIvy.NewMessageEventHandler(CommunicationIvy_OnNewMessage);
                return true;
            }
            else
            {
                updateConsole("La creation du BUS Ivy a échoué");
                return false;
            }
        }

        private void CommunicationIvy_OnNewMessage(object sender, MessageEventArgs e)
        {
            String bufferType = "STArduino";
            MessageIvy m = new MessageIvy();
            m.RemplireMessage(e.Message);
            //Le capteur ne detecte personne
            if (m.distance >= 666)
            {
                //Console.WriteLine("Le capteur ne detecte personne");
                Personne p = personnes.getPersonneByCapteur(m.numeroCapteur);
                //Ce capteur appartient à une personne
                if (p != null)
                {
                    //Ajout au buffer la valeur du capteur
                    buffer.addsensorvalue(bufferType, m.numeroCapteur, m.distance);
                    //Le buffer contient trop de message d'abscence 
                    if (buffer.nbSensorValue(bufferType, m.numeroCapteur) > this.nbAbsenceAcceptee)
                    {
                        buffer.clearSensorValue(bufferType, m.numeroCapteur);
                        //Suppression du capteur seulement si il se trouve à l'extremité de la liste de l'utilisateur
                        if (p.capteurAuExtremite(m.numeroCapteur))
                        {
                            p.supprimerCapteur(m.numeroCapteur);
                            //Suppression de la personne si elle n'a plus de capteur dans sa liste
                            if (!p.hasCateurs())
                            {
                                MessageIvyPresence messpresence = new MessageIvyPresence(p.identifiant, false);
                                ivybus.msgsend(messpresence);
                                personnes.supprimerPersonne(p);
                            }
                            else
                            {
                                genererPositionPersonne(p);
                                MessageIvyPosition messposition = new MessageIvyPosition(p.identifiant, p.x, p.y, p.get_distance());
                                ivybus.msgsend(messposition);
                            }
                        }

                    }
                }

            }
            //Le capteur detecte quelqu'un
            else
            {
                //Suppression des messages d'abscence du buffer
                buffer.clearSensorValue(bufferType, m.numeroCapteur);
                //Aucune personne ne possède ce capteur
                Personne p = personnes.getPersonneByCapteur(m.numeroCapteur);
                if (p == null)
                {
                    Personne p_proche = personnes.getPersonneByCapteurProche(m.numeroCapteur, reglageInterval);
                    //Aucune personne proche du capteur
                    if (p_proche == null)
                    {
                        Personne new_p = new Personne();
                        new_p.ajouterCapteur(m.numeroCapteur, m.distance);
                        personnes.addPersonne(new_p);
                        genererPositionPersonne(new_p);
                        //Envoi de l'evenement
                        MessageIvyPresence messpresence = new MessageIvyPresence(new_p.identifiant, true);
                        ivybus.msgsend(messpresence);
                        MessageIvyPosition messposition = new MessageIvyPosition(new_p.identifiant, new_p.x, new_p.y, new_p.get_distance());
                        ivybus.msgsend(messposition);
                    }
                    //Une personne est proche du capteur
                    else
                    {
                        //La personne est à une distance proche de celle signalée par le capteur
                        if (Math.Abs(m.distance - p_proche.get_distance()) < distanceDifferenceAcceptee)
                        {
                            p_proche.ajouterCapteur(m.numeroCapteur, m.distance);
                            genererPositionPersonne(p_proche);
                            MessageIvyPosition messposition = new MessageIvyPosition(p_proche.identifiant, p_proche.x, p_proche.y, p_proche.get_distance());
                            ivybus.msgsend(messposition);
                        }
                        //La personne est eloignée de la distance proposé
                        else
                        {
                            Personne new_p = new Personne();
                            new_p.ajouterCapteur(m.numeroCapteur, m.distance);
                            personnes.addPersonne(new_p);
                            genererPositionPersonne(new_p);
                            MessageIvyPresence messpresence = new MessageIvyPresence(new_p.identifiant, true);
                            ivybus.msgsend(messpresence);
                            MessageIvyPosition messposition = new MessageIvyPosition(new_p.identifiant, new_p.x, new_p.y, new_p.get_distance());
                            ivybus.msgsend(messposition);
                        }
                    }
                }
                //Une personne possède ce capteur dans sa liste
                else
                {
                    //La personne est à une distance proche de celle signalée par le capteur
                    if (Math.Abs(m.distance - p.get_distance()) < distanceDifferenceAcceptee)
                    {
                        p.updateCapteur(m.numeroCapteur, m.distance);
                        MessageIvyPosition messposition = new MessageIvyPosition(p.identifiant, p.x, p.y, p.get_distance());
                        ivybus.msgsend(messposition);
                    }
                    //La personne est eloignée de la distance proposé
                    else
                    {
                        p.supprimerCapteur(m.numeroCapteur);
                        //Envoi de l'evenement
                        if (!p.hasCateurs())
                        {
                            MessageIvyPresence messpresence = new MessageIvyPresence(p.identifiant, false);
                            ivybus.msgsend(messpresence);
                            personnes.supprimerPersonne(p);
                        }
                        else
                        {
                            genererPositionPersonne(p);
                            MessageIvyPosition messposition = new MessageIvyPosition(p.identifiant, p.x, p.y, p.get_distance());
                            ivybus.msgsend(messposition);
                        }

                        Personne new_p = new Personne();
                        new_p.ajouterCapteur(m.numeroCapteur, m.distance);
                        personnes.addPersonne(new_p);
                        genererPositionPersonne(new_p);
                        MessageIvyPresence messpresence2 = new MessageIvyPresence(new_p.identifiant, false);
                        ivybus.msgsend(messpresence2);
                        MessageIvyPosition messposition2 = new MessageIvyPosition(new_p.identifiant, new_p.x, new_p.y, new_p.get_distance());
                        ivybus.msgsend(messposition2);
                    }
                }
            }
            //Affichage debug
            //foreach (Personne p in personnes.liste_personnes.Values)
            //{
            //    Console.WriteLine(p.ToString());
            //}
            //Console.WriteLine("");
        }

        //Mise a jour de la console
        public void updateConsole(String s)
        {
            try { this.Invoke(this.ConsoleDelegate, new object[] { s }); }
            catch (ObjectDisposedException) { }
        }

        //Routine d'ecriture sur la console
        private void ecrireSurConsole(String s)
        {
            List<string> l = TextConsole.Lines.ToList(); ;
            l.Add(s);
            TextConsole.Lines = l.ToArray();
            TextConsole.SelectionStart = TextConsole.Text.Length;
            TextConsole.SelectionLength = 0;
            TextConsole.ScrollToCaret();
        }

        //Stop la  connexion au port
        private void stopper_Click(object sender, EventArgs e)
        {

            if (ivybus != null)
            {
                updateConsole("Fermeture du bus de donné ivy...");
                ivybus.closeIvyBus();
                updateConsole("Fermeture du bus ivy : ok");
            }
            updateConsole("Stop ok");
            personnes = new Personnes();
            buffer = new Buffer();
            this.Valider.Enabled = true;
            this.stopper.Enabled = false;
        }

    }
}
